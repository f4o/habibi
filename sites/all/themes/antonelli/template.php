<?php

/**
 * Implements template_preprocess_page().
 */
function antonelli_preprocess_page(&$vars) {
  // View always adds title in breadcrumb and we don't want that.
  $menu_item = menu_get_item();
  if ($menu_item['page_callback'] == 'views_page') {
    drupal_set_title('');
  }

  if (_habibi_tt_already_started()) {
    $vars["habibi_tt"] = TRUE;
  }
  else {
    $vars["habibi_tt"] = FALSE;
  }
}

/**
 * Implements theme_preprocess_views_calc_table().
 */
function antonelli_preprocess_views_calc_table(&$vars) {
  // unset($vars["sub_totals"]);
  $seconds = $vars["totals"]["SUM"]["duration"];
  $vars["totals"]["SUM"]["duration"] = format_interval($seconds, 2);
}

function antonelli_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode('<span class="glue">»</span>', $breadcrumb) . '</div>';
    return $output;
  }
}

function antonelli_links($variables) {
  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = array($key);

      // Add first, last and active classes to the list of links to help out
      // themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
        && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class[] = 'active';
      }

      if ($link["href"] == 'requests' || $link["href"] == 'done') {
        $class[] = 'right';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for
        // adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $link['title'] . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Implements template_preprocess_views_view_field().
 */
function antonelli_preprocess_views_view_field(&$vars) {
  if ($vars["view"]->name == 'doing' && $vars["view"]->current_display == 'page') {
    $is_title = $vars["field"]->real_field == 'title';
    $is_child = $vars["row"]->field_data_field_story_ref_field_story_ref_target_id;
    if ($is_title && $is_child) {
      $vars["output"] = '<span class="child-indentation"></span>' . $vars["output"];
    }
  }
}

/**
 * Implements theme_field__field_type().
 * It has H3 wrapper in bartik. This is not what we want.
 */
function antonelli_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label">' . $variables['label'] . ': </div>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}