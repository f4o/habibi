<?php

/**
 * Page callback for /message/$message.
 */
function habibi_message_page($message) {

  $form = drupal_get_form('habibi_message_noted_form', $message);

  $output = $message->arguments["!comment_message"];
  $output .= $message->arguments["!node_link"];
  $output .= drupal_render($form);

  return $output;
}

/**
 * Form to mark message as noted.
 */
function habibi_message_noted_form($form, &$form_state, $message) {
  $form['#message'] = $message;

  $form['actions'] = [
    '#type' => 'actions',
    '#prefix' => '<hr class="message-divider">',
  ];

  $form['actions']['noted'] = [
    '#type' => 'submit',
    '#value' => t('Mark as noted'),
  ];

  return $form;
}

/**
 * Submit handler for message form.
 */
function habibi_message_noted_form_submit($form, &$form_state) {
  $message = $form["#message"];
  $wrapper = entity_metadata_wrapper('message', $message);
  $wrapper->field_noted->set(TRUE);
  $wrapper->save();
}