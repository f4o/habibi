<?php

/**
 * Implements hook_node_presave().
 */
function habibi_node_presave($node) {

  // Update stage for children
  if (!$node->is_new && $node->type == 'story') {
    $wrapper_parent = entity_metadata_wrapper('node', $node);
    if ($children = _habibi_get_story_children($node->nid)) {
      foreach ($children as $nid => $child) {
        $wrapper_child = entity_metadata_wrapper('node', $child);
        $wrapper_child->field_request_ref->set($wrapper_parent->field_request_ref->value());
        $wrapper_child->field_project_ref->set($wrapper_parent->field_project_ref->value());
        $wrapper_child->field_priority->set($wrapper_parent->field_priority->value());
        $wrapper_child->field_story_stage->set($wrapper_parent->field_story_stage->value());
        $wrapper_child->field_stage_note->set($wrapper_parent->field_stage_note->value());
        $wrapper_child->field_delivery_goal->set($wrapper_parent->field_delivery_goal->value());
        $wrapper_child->save();
      }
    }

    // Preserve original ETA
    // @todo - use entity_metadata_wrapper.
    $previous_tid = $node->original->field_story_stage['und'][0]['tid'];
    $current_tid = $node->field_story_stage['und'][0]['tid'];

    $is_previous_backlog = $previous_tid == BACKLOG_STAGE_TID;
    $is_current_wfa = $current_tid == WAITING_APPROVAL_STAGE_TID;
    $eta_original_empty = empty($node->original->field_estimate_original['und'][0]['from']);

    if ($is_current_wfa && $is_previous_backlog && $eta_original_empty) {
      $node->field_estimate_original['und'][0]['from'] = $node->field_estimate['und'][0]['from'];
      $node->field_estimate_original['und'][0]['to'] = $node->field_estimate['und'][0]['to'];
    }
  }
//  if ($node->type == 'task') {
//    $task_eta_from = $node->field_estimate['und'][0]['from'];
//    $task_eta_to = $node->field_estimate['und'][0]['to'];
//    if (!$node->is_new) {
//      $task_eta_old_from = $node->original->field_estimate['und'][0]['from'];
//      $task_eta_old_to = $node->original->field_estimate['und'][0]['to'];
//      $task_eta_from = $task_eta_from - $task_eta_old_from;
//      $task_eta_to = $task_eta_to - $task_eta_old_to;
//    }
//    _habibi_update_story_eta($node, $task_eta_from, $task_eta_to);
//  }
//
//  if ($node->type == 'story' && !$node->is_new) {
//    // Preserve original ETA.
//    $current_tid = $node->field_story_stage['und'][0]['tid'];
//    $previous_tid = $node->original->field_story_stage['und'][0]['tid'];
//    $is_current_backlog = _habibi_get_parent_story_stage_tid($current_tid) == DOING_STAGE_TID;
//    $is_previous_definition = _habibi_get_parent_story_stage_tid($previous_tid) == BACKLOG_STAGE_TID;
//    $eta_original_empty = empty($node->original->field_estimate_original['und'][0]['from']);
//    if ($is_current_backlog && $is_previous_definition && $eta_original_empty) {
//      _habibi_preserve_eta($node);
//    }
//  }
//
//  if (!$node->is_new) {
//    $values = [
//      'arguments' => [
//        '!what_exactly' => 'Some content manipulation happened',
//      ],
//      'uid' => $node->uid,
//    ];
//    $message = message_create('node_stream', $values);
//
//    $wrapper = entity_metadata_wrapper('message', $message);
//    $wrapper->field_mess_node_ref->set($node);
//    $wrapper->save();
//  }
}

/**
 * Implements hook_node_delete().
 */
function habibi_node_delete($node) {
  if ($node->type == 'task') {
    $task_eta_from = - $node->field_estimate['und'][0]['from'];
    $task_eta_to = - $node->field_estimate['und'][0]['to'];
    _habibi_update_story_eta($node, $task_eta_from, $task_eta_to);
  }
}

/**
 * Custom function to update story eta field.
 *
 * @param stdClass $story
 */
function _habibi_update_story_eta($task, $task_eta_from, $task_eta_to) {
  $story_id = $task->field_story_ref['und'][0]['target_id'];


  $story = node_load($story_id);
  if ($story->field_estimate) {
    $story_eta_from = $story->field_estimate['und'][0]['from'];
    $story_eta_to = $story->field_estimate['und'][0]['to'];
  }
  else {
    $story_eta_from = 0;
    $story_eta_to = 0;
  }

  $story_eta_new_from = $story_eta_from + $task_eta_from;
  $story_eta_new_to = $story_eta_to + $task_eta_to;
  $story->field_estimate['und'][0]['from'] = $story_eta_new_from;
  $story->field_estimate['und'][0]['to'] = $story_eta_new_to;

  field_attach_presave('node', $story);
  field_attach_update('node', $story);
  entity_get_controller('node')->resetCache(array($story->nid));
}

/**
 * Store original ETA on story and task level.
 *
 * @param stdClass $story
 */
function _habibi_preserve_eta($story) {
  // User story level.
  $story->field_estimate_original['und'][0]['from'] = $story->field_estimate['und'][0]['from'];
  $story->field_estimate_original['und'][0]['to'] = $story->field_estimate['und'][0]['to'];

  node_save($story);

//  if ($tasks = _habibi_get_story_children($story->nid)) {
//    foreach ($tasks as $nid => $task) {
//      $task->field_estimate_original['und'][0]['from'] = $task->field_estimate['und'][0]['from'];
//      $task->field_estimate_original['und'][0]['to'] = $task->field_estimate['und'][0]['to'];
//
//      field_attach_presave('node', $task);
//      field_attach_update('node', $task);
//      entity_get_controller('node')->resetCache(array($task->nid));
//    }
//  }
}

/**
 * Return array of nodes with the same field_story_ref value.
 */
function _habibi_get_story_children($nid) {
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'story')
    ->fieldCondition('field_story_ref', 'target_id', $nid, '=')
    // Run the query as user 1.
    ->addMetaData('account', user_load(1));

  $result = $query->execute();
  if (isset($result['node'])) {
    $nids = array_keys($result['node']);
    $stories = node_load_multiple($nids);

    return $stories;
  }
  return [];
}

/**
 * Check and return parent tid.
 *
 * @param $tid
 * @return int
 */
function _habibi_get_parent_story_stage_tid($tid) {
  $parents = taxonomy_get_parents_all($tid);
  $term = end($parents);
  return $term->tid;
}