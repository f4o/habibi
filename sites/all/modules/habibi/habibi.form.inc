<?php

/**
 * Implements hook_form_alter().
 */
function habibi_form_alter(&$form, &$form_state, $form_id) {

  $zen_forms = [
    'request_node_form',
    'story_node_form',
    'project_node_form',
    'feature_node_form',
  ];

  if (in_array($form_id, $zen_forms)) {
    $form["revision_information"]["revision"]['#access'] = FALSE;
    $form['author']['#access'] = FALSE;
    $form['comment_settings']['#access'] = FALSE;
    $form['options']['#access'] = FALSE;
    $form['path']['#access'] = FALSE;

    $form['#attached']['css'] = array(
      drupal_get_path('theme', 'antonelli') . '/css/habibi_admin.css',
    );
  }

  // Hide estimate fields since they are derived from task estimates.
  // $form['field_estimate']['#access'] = FALSE;
  // $form['field_estimate_original']['#access'] = FALSE;

  // Story node type.
  if ($form_id == 'story_node_form') {

    $form["field_estimate_original"]['#access'] = FALSE;

    // If it's new story.
    if (empty($form['#node']->nid)) {
      drupal_set_title('Create new node');
      $form["field_percentage_done"]['#access'] = FALSE;
      $form["field_story_ref"]['#access'] = FALSE;
      if (isset($_GET['story_ref'])) {
        drupal_set_title('Create new task');
        $node = node_load($_GET['story_ref']);
        $wrapper_parent = entity_metadata_wrapper('node', $node);
        $form["field_type"]['#access'] = FALSE;
        $form["field_type"]["und"]["#default_value"] = 'task';
        $form["field_story_ref"]["und"][0]["target_id"]['#default_value'] = "$node->title ($node->nid)";


        ////////////////////////////////////////////////////////////////////////
        // Automatic attach parent story "taxonomy" values.
        ////////////////////////////////////////////////////////////////////////

        // Autocomplete
        $nid = $wrapper_parent->field_request_ref->nid->value();
        $title = $wrapper_parent->field_request_ref->title->value();
        $request = "$title ($nid)";
        $form["field_request_ref"]["und"][0]["target_id"]['#default_value'] = $request;

        // OK - Radio buttons
        $form["field_project_ref"]["und"]["#default_value"] = $wrapper_parent->field_project_ref->nid->value();

        // OK - Radio buttons
        $form["field_priority"]["und"]["#default_value"] = $wrapper_parent->field_priority->value();

        // OK - Radio buttons
        $form["field_story_stage"]["und"]["#default_value"] = $wrapper_parent->field_story_stage->tid->value();

        // OK - Radio buttons
        $form["field_delivery_goal"]["und"]["#default_value"] = $wrapper_parent->field_delivery_goal->value();

        // OK - Stage note
        $form["field_stage_note"]["und"][0]["value"]["#default_value"] = $wrapper_parent->field_stage_note->value();

        // Finally hide form elements.
        $form["field_request_ref"]['#access'] = FALSE;
        $form["field_project_ref"]['#access'] = FALSE;
        $form["field_priority"]['#access'] = FALSE;
        $form["field_story_stage"]['#access'] = FALSE;
        $form["field_delivery_goal"]['#access'] = FALSE;
        $form["field_stage_note"]['#access'] = FALSE;
      }
    }
    // If it's existing story.
    else {
      drupal_set_title('Edit ' . $form["#node"]->field_type["und"][0]["value"]);
      $form["field_type"]['#access'] = FALSE;
      $form["field_story_ref"]['#access'] = FALSE;

      // Hide for elements for story children that should be the same like parent.
      if (isset($form["#node"]->field_story_ref["und"][0]["target_id"])) {
        $form["field_request_ref"]['#access'] = FALSE;
        $form["field_project_ref"]['#access'] = FALSE;
        $form["field_priority"]['#access'] = FALSE;
        $form["field_story_stage"]['#access'] = FALSE;
        $form["field_delivery_goal"]['#access'] = FALSE;
        $form["field_stage_note"]['#access'] = FALSE;
      }
    }

  }

  if ($form_id == 'views_exposed_form') {
    $forms_with_project = [
      'views-exposed-form-requests-page',
      'views-exposed-form-definition-page',
      'views-exposed-form-backlog-page',
      'views-exposed-form-doing-page',
      'views-exposed-form-done-page',
      'views-exposed-form-feature-files-page',
    ];
    if (in_array($form["#id"], $forms_with_project)) {
      $options = [null => '- Any -'];
      $options += _habibi_get_my_projects();

      $form['project']['#type'] = 'select';
      $form['project']['#multiple'] = FALSE;
      $form['project']['#options'] = $options;
      $form['project']['#size'] = null;
    }
  }
}