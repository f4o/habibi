<?php

class HabibiACEntityController extends EntityAPIController {

  function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $build = parent::buildContent($entity, $view_mode, $langcode, $content); // TODO: Change the autogenerated stub

    $build['ac_body'] = array(
      '#type' => 'markup',
      '#markup' => check_markup($entity->ac_body, 'filtered_html'), // $entity->ac_body,
      '#prefix' => '<div class="habibi-ac-body">',
      '#suffix' => '</div>',
    );

    return $build;
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;
    if (isset($entity->is_new)) {
      $entity->uid = $user->uid;
      $entity->timestamp = REQUEST_TIME;
    }

    return parent::save($entity, $transaction);
  }
}