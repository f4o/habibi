<?php

class HabibiACInlineEntityFormController extends EntityInlineEntityFormController {

  /**
   * Overrides EntityInlineEntityFormController::entityForm().
   */
  public function entityForm($entity_form, &$form_state) {
    $ac = $entity_form["#entity"];
    if ($ac->is_new) {
      $entity_form['nid'] = [
        '#type' => 'hidden',
        '#default_value' => $form_state["node"]->nid,
      ];
    }
    $entity_form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#size' => 60,
      '#default_value' => isset($ac->title) ? $ac->title : '', //isset($ac->ac_body) ? $ac->body : '',
      '#required' => TRUE,
    );
    $entity_form['ac_body'] = array(
      '#type' => 'textarea',
      '#title' => t('Body'),
      '#rows' => 7,
      '#default_value' => isset($ac->ac_body) ? $ac->ac_body : '', //isset($ac->ac_body) ? $ac->body : '',
      '#required' => TRUE,
    );

    return $entity_form;
  }
}