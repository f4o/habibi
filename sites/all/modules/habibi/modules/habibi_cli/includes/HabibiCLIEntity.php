<?php

class HabibiCLIEntity extends Entity {

  protected function defaultUri() {
    return array('path' => 'cli/' . $this->identifier());
  }
}