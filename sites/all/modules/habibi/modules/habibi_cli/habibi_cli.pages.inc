<?php

/**
 * Implements hook_form_alter().
 */

function habibi_clis_form($form, &$form_state, $node) {

  $header = [
    'title' => 'Title',
    'operations' => 'Operations',
  ];

  $options = [];
  $clis = _habibi_cli_get_clis($node->nid);
  foreach ($clis as $cliid => $cli) {
    $options[$cliid] = [
      'title' => _habibi_cli_get_title_data($cli),
      'operations' => _habibi_cli_get_operation_links($cli),
    ];
  }

  $form['nid'] = [
    '#type' => 'hidden',
    '#value' => $node->nid,
  ];

  $form['clis'] = [
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#multiple' => TRUE,
    '#empty' => t('No check list items available.'),
    '#js_select' => FALSE,
  ];

  // Action buttons
//  $form['actions'] = [
//    '#type' => 'actions',
//    '#weight' => 99,
//    'submit' => [
//      '#type' => 'submit',
//      '#value' => t('Switch status of selected checklist item'),
//      ],
//  ];

  $form['add_new'] = [
    '#type' => 'fieldset',
    '#weight' => 100,
    '#attributes' => array('class' => array('container-inline'))
  ];

  $form['add_new']['title'] = [
    '#type' => 'textfield',
    '#size' => 128,
    '#maxlength' => 255,
  ];

  $form['add_new']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Add new checklist item'),
    '#submit' => ['habibi_cli_form_add_new_submit'],
  ];

  return $form;
}

function habibi_cli_form_add_new_submit($form, &$form_state) {
  $values = [
    'title' => $form_state["values"]["title"],
    'nid' => $form_state['values']['nid'],
  ];
  $cli = entity_create('habibi_cli', $values);

  entity_save('habibi_cli', $cli);
}

function habibi_clis_form_submit($form, &$form_state) {
  $x = 0;
}


function _habibi_cli_get_clis($nid) {
  $clis = [];
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'habibi_cli')
    ->propertyCondition('nid', $nid, '=');

  $result = $query->execute();
  if (isset($result['habibi_cli'])) {
    $ids = array_keys($result['habibi_cli']);
    $clis = entity_load('habibi_cli', $ids);
  }

  return $clis;
}

function _habibi_cli_get_operation_links($cli) {
  global $user;
  $output = '';
  $items = [];

  // @todo check uid for edit and delete. Only owner can do that.
  if ($user->uid == $cli->created_by) {
    $items[] = l('Edit', 'admin/cli/manage/' . $cli->cliid, [
      'query' => [
        'destination' => current_path()
      ],
    ]);

    $items[] = l('Delete', 'admin/cli/manage/' . $cli->cliid . '/delete', [
      'query' => [
        'destination' => current_path()
      ],
    ]);
  }

  $values = [
    'items' => $items,
    'type' => 'ul',
    'attributes' => [
      'class' => ['links', 'inline'],
    ],
  ];

  return theme('item_list', $values);
}

function _habibi_cli_get_title_data($cli) {
  $user = user_load($cli->created_by);
  $output = '<div class="td-big-text">' . $cli->title . '</div>';
  $output .= '<div class="td-small-text">Created ' . format_date($cli->created_at, 'short') . ' by ' . l($user->name, 'user/' . $user->uid) . '</div>';
  return $output;
}