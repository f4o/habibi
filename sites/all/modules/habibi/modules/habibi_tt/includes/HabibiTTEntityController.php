<?php

class HabibiTTEntityController extends EntityAPIController {

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    global $user;
    if (isset($entity->is_new)) {
      $entity->uid = $user->uid;
      if (empty($entity->time)) {
        $entity->time = REQUEST_TIME;
      }
    }

    return parent::save($entity, $transaction);
  }
}