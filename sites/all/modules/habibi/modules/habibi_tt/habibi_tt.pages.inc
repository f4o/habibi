<?php

/**
 * Page callback for node/%nid/tt.
 */

/**
 * Page callback for node/%node/tt.
 */
function habibi_tt_tracks($node) {
  $retroative_form = drupal_get_form('habibi_tt_retroactive_form', $node);
  return drupal_render($retroative_form);

  // Custom view is also attached below this form.
}

/**
 * Form to submit time track retroactively.
 */
function habibi_tt_retroactive_form($form, &$form_state, $node) {
  $form['nid'] = [
    '#type' => 'hidden',
    '#value' => $node->nid,
  ];

  $form['retroactive'] = [
    '#type' => 'fieldset',
    '#title' => 'Retroactive time track submit',
    '#weight' => 100,
    '#attributes' => array('class' => array('container-inline')),
  ];

  $form['retroactive']['date'] = [
    '#type' => 'date',
  ];

  $form['retroactive']['hours'] = [
    '#type' => 'textfield',
    '#suffix' => ' hours',
    '#size' => 9,
  ];

  $form['retroactive']['note'] = [
    '#type' => 'textfield',
    '#size' => 56,
    '#attributes' => [
      'placeholder' => t('Note about next time track'),
    ],
  ];

  $form['retroactive']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Add'),
  ];

  return $form;
}

/**
 * Sumbit handler to add time track retroactively.
 */
function habibi_tt_retroactive_form_submit($form, &$form_state) {
  $date = $form_state["values"]["date"];
  $date_format = $date['year'] . '-' . $date['month'] . '-' . $date['day'];
  $timestamp = strtotime($date_format);
  $duration = (float)$form_state["values"]["hours"] * 60 * 60;
  $note = $form_state["values"]["note"];
  $values = [
    'nid' => $form_state['values']['nid'],
    'time' => $timestamp,
    'duration' => $duration,
    'note' => $note,
  ];
  $tt = entity_create('habibi_tt', $values);

  entity_save('habibi_tt', $tt);
}