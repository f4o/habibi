<?php

/**
 * Implements hook_block_view().
 */
function habibi_block_view($delta = '') {
  $block = [];

  switch ($delta) {
    case 'habibi_done':
      $block['subject'] = t('% Done');
      $form = drupal_get_form('habibi_done_form');
      $block['content'] = drupal_render($form);
      break;

    case 'habibi_request_stage':
      $block['subject'] = t('Stage management');
      $form = drupal_get_form('habibi_request_stage_form');
      $block['content'] = drupal_render($form);
      break;
  }

  return $block;
}

/**
 * Custom form for block view.
 */
function habibi_done_form($form, &$form_state) {
  $options = [
    0 => '0%',
    10 => '10%',
    20 => '20%',
    30 => '30%',
    40 => '40%',
    50 => '50%',
    60 => '60%',
    70 => '70%',
    80 => '80%',
    90 => '90%',
    100 => '100%',
  ];

  $node = menu_get_object('node');
  $default = $node ? $node->field_percentage_done["und"][0]["value"] : 0;

  $form['#node'] = $node;

  $form['percentage'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $default,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form submit handler to store selected % done.
 */
function habibi_done_form_submit($form, &$form_state) {
  if ($node = $form['#node']) {
    $node->field_percentage_done['und'][0]['value'] = $form_state['values']['percentage'];
    node_save($node);
  }
}

/**
 * Form for request stage manipulation.
 */
function habibi_request_stage_form($form, &$form_state) {
  $options = [];
  $terms = [];
  $terms = taxonomy_get_tree(REQUEST_STAGES_VID, 0, null, TRUE);
  $node = menu_get_object();

  foreach ($terms as $term) {
    $options[$term->tid] = $term->name;
  }

  $form['#node'] = $node;
  $form['stages'] = [
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => $node->field_request_stage["und"][0]["tid"],
  ];
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * Form submit handler to store request stage.
 */
function habibi_request_stage_form_submit($form, &$form_state) {
  if ($node = $form['#node']) {
    $node->field_request_stage['und'][0]['tid'] = $form_state['values']['stages'];
    node_save($node);
  }
}