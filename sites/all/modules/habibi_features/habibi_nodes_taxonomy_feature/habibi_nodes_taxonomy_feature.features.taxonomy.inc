<?php
/**
 * @file
 * habibi_nodes_taxonomy_feature.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function habibi_nodes_taxonomy_feature_taxonomy_default_vocabularies() {
  return array(
    'digital_products' => array(
      'name' => 'Digital products',
      'machine_name' => 'digital_products',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -10,
    ),
    'request_stages' => array(
      'name' => 'Request stages',
      'machine_name' => 'request_stages',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -9,
    ),
    'story_stages' => array(
      'name' => 'Story stages',
      'machine_name' => 'story_stages',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -8,
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => 'Use tags to group articles on similar topics into categories.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => -6,
    ),
  );
}
