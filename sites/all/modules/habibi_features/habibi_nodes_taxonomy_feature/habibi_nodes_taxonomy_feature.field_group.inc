<?php
/**
 * @file
 * habibi_nodes_taxonomy_feature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function habibi_nodes_taxonomy_feature_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_acceptance_criterias|node|story|default';
  $field_group->group_name = 'group_acceptance_criterias';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_assets';
  $field_group->data = array(
    'label' => 'Specification',
    'weight' => '36',
    'children' => array(
      0 => 'field_acceptance_criterias',
      1 => 'field_features_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Specification',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_acceptance_criterias|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assets2|node|story|default';
  $field_group->group_name = 'group_assets2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_assets';
  $field_group->data = array(
    'label' => 'Assets',
    'weight' => '38',
    'children' => array(
      0 => 'field_assets_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $field_groups['group_assets2|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assets|node|request|default';
  $field_group->group_name = 'group_assets';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_group';
  $field_group->data = array(
    'label' => 'Assets',
    'weight' => '15',
    'children' => array(
      0 => 'field_asset',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-assets field-group-tab',
      ),
    ),
  );
  $field_groups['group_assets|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assets|node|request|form';
  $field_group->group_name = 'group_assets';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Assets',
    'weight' => '5',
    'children' => array(
      0 => 'field_asset',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-assets field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_assets|node|request|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assets|node|story|default';
  $field_group->group_name = 'group_assets';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group',
    'weight' => '3',
    'children' => array(
      0 => 'group_assets2',
      1 => 'group_acceptance_criterias',
      2 => 'group_eta',
      3 => 'group_assigneese',
      4 => 'group_info',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_assets|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assigneese|node|story|default';
  $field_group->group_name = 'group_assigneese';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_assets';
  $field_group->data = array(
    'label' => 'People',
    'weight' => '34',
    'children' => array(
      0 => 'field_users_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'People',
      'instance_settings' => array(
        'classes' => 'group-assigneese field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_assigneese|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_assigneese|node|story|form';
  $field_group->group_name = 'group_assigneese';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'People',
    'weight' => '4',
    'children' => array(
      0 => 'field_users_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'People',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-assigneese field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_assigneese|node|story|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_docs|node|request|default';
  $field_group->group_name = 'group_docs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_group';
  $field_group->data = array(
    'label' => 'Specification',
    'weight' => '14',
    'children' => array(
      0 => 'field_features_ref',
      1 => 'field_acceptance_criterias',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Specification',
      'instance_settings' => array(
        'classes' => 'group-docs field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_docs|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_docs|node|request|form';
  $field_group->group_name = 'group_docs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Specification',
    'weight' => '3',
    'children' => array(
      0 => 'field_features_ref',
      1 => 'field_acceptance_criterias',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Specification',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-docs field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_docs|node|request|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_docs|node|story|form';
  $field_group->group_name = 'group_docs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Specification',
    'weight' => '6',
    'children' => array(
      0 => 'field_acceptance_criterias',
      1 => 'field_features_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Specification',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-docs field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_docs|node|story|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eta|node|story|default';
  $field_group->group_name = 'group_eta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_assets';
  $field_group->data = array(
    'label' => 'Time',
    'weight' => '35',
    'children' => array(
      0 => 'field_expected_delivery',
      1 => 'field_estimate',
      2 => 'field_estimate_original',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Time',
      'instance_settings' => array(
        'classes' => 'group-eta field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_eta|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_eta|node|story|form';
  $field_group->group_name = 'group_eta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Time',
    'weight' => '5',
    'children' => array(
      0 => 'field_expected_delivery',
      1 => 'field_estimate',
      2 => 'field_estimate_original',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Time',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-eta field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_eta|node|story|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_group|node|request|default';
  $field_group->group_name = 'group_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Group',
    'weight' => '1',
    'children' => array(
      0 => 'group_people',
      1 => 'group_time',
      2 => 'group_info',
      3 => 'group_assets',
      4 => 'group_docs',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-group field-group-tabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_group|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|node|request|default';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_group';
  $field_group->data = array(
    'label' => 'Info',
    'weight' => '11',
    'children' => array(
      0 => 'field_desc_1',
      1 => 'field_products_ref',
      2 => 'field_priority',
      3 => 'field_request_stage',
      4 => 'field_project_ref',
      5 => 'field_stage_note',
      6 => 'field_tags_ref',
      7 => 'field_desc_2',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-info field-group-tab',
      ),
    ),
  );
  $field_groups['group_info|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|node|request|form';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Info',
    'weight' => '0',
    'children' => array(
      0 => 'field_desc_1',
      1 => 'field_products_ref',
      2 => 'field_priority',
      3 => 'field_request_stage',
      4 => 'field_project_ref',
      5 => 'field_stage_note',
      6 => 'field_tags_ref',
      7 => 'field_desc_2',
      8 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-info field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_info|node|request|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info|node|story|default';
  $field_group->group_name = 'group_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_assets';
  $field_group->data = array(
    'label' => 'Info',
    'weight' => '33',
    'children' => array(
      0 => 'field_story_stage',
      1 => 'field_desc_1',
      2 => 'field_project_ref',
      3 => 'field_priority',
      4 => 'field_desc_2',
      5 => 'field_products_ref',
      6 => 'field_stage_note',
      7 => 'field_delivery_goal',
      8 => 'field_request_ref',
      9 => 'field_percentage_done',
      10 => 'field_tags_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-info field-group-tab',
      ),
    ),
  );
  $field_groups['group_info|node|story|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_people|node|request|default';
  $field_group->group_name = 'group_people';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_group';
  $field_group->data = array(
    'label' => 'People',
    'weight' => '12',
    'children' => array(
      0 => 'field_user_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-people field-group-tab',
      ),
    ),
  );
  $field_groups['group_people|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_people|node|request|form';
  $field_group->group_name = 'group_people';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'People',
    'weight' => '1',
    'children' => array(
      0 => 'field_user_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-people field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_people|node|request|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_time|node|request|default';
  $field_group->group_name = 'group_time';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_group';
  $field_group->data = array(
    'label' => 'Time',
    'weight' => '13',
    'children' => array(
      0 => 'field_expected_delivery',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-time field-group-tab',
      ),
    ),
  );
  $field_groups['group_time|node|request|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_time|node|request|form';
  $field_group->group_name = 'group_time';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'request';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Time',
    'weight' => '2',
    'children' => array(
      0 => 'field_expected_delivery',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-time field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_time|node|request|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_user_story|node|story|form';
  $field_group->group_name = 'group_user_story';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Info',
    'weight' => '2',
    'children' => array(
      0 => 'field_story_stage',
      1 => 'field_desc_1',
      2 => 'field_project_ref',
      3 => 'field_priority',
      4 => 'field_desc_2',
      5 => 'field_products_ref',
      6 => 'field_stage_note',
      7 => 'field_delivery_goal',
      8 => 'field_request_ref',
      9 => 'field_percentage_done',
      10 => 'field_tags_ref',
      11 => 'title',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Info',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-user-story field-group-tab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_user_story|node|story|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_wireframes|node|story|form';
  $field_group->group_name = 'group_wireframes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'story';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Assets',
    'weight' => '7',
    'children' => array(
      0 => 'field_assets_ref',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Assets',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $field_groups['group_wireframes|node|story|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Assets');
  t('Group');
  t('Info');
  t('People');
  t('Specification');
  t('Time');

  return $field_groups;
}
